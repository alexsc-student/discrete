import java.util.ArrayList;

 class PrimeNumberGeneration {
    static ArrayList<Integer> sieveOfEratosthenes(int n){
        boolean[] isPrime=new boolean[n+1];
        for(int i=0;i<=n;i++){
            isPrime[i]=true;
        }
        ArrayList<Integer> primes=new ArrayList<>();
        isPrime[0]=false;
        if(n>1)isPrime[1]=false;
        for (int i=2;i*i<=n;i++){
            if(isPrime[i]){
                for (int j=i*i;j<=n;j+=i){
                    isPrime[j]=false;
                }
            }
        }
        for (int i=0;i<=n;i++) {
            if(isPrime[i]){
                primes.add(i);

            }
        }
        return primes;

    }
}
