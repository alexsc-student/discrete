import java.math.BigInteger;
import java.util.ArrayList;

 class ChineseRemainderTheorem {

     static class SumMultiply {
        private long sum;
        private long multiply;

        private SumMultiply(long sum, long multiply) {
            this.sum = sum;
            this.multiply = multiply;
        }

         long getSum() {
            return sum;
        }

         long getMultiply() {
            return multiply;
        }
    }

    static SumMultiply CRT(BigInteger A, BigInteger B, long[] relativelyPrimes) {

        long C = 0, D = 0, M = 1;
        long[] a = new long[relativelyPrimes.length];
        long[] b = new long[relativelyPrimes.length];
        long[] c = new long[relativelyPrimes.length];
        long[] d = new long[relativelyPrimes.length];
        ArrayList<Long> modInverse = new ArrayList<>();
        for (int i = 0; i < relativelyPrimes.length; i++) {
            a[i] = (A.mod(BigInteger.valueOf(relativelyPrimes[i]))).longValue();
            b[i] = (B.mod(BigInteger.valueOf(relativelyPrimes[i]))).longValue();
            c[i] = ((a[i] + b[i]) % relativelyPrimes[i]);
            d[i] = ((a[i] * b[i]) % relativelyPrimes[i]);
        }
        for (long relativelyPrime : relativelyPrimes) {
            M = M * relativelyPrime;
        }
        for (long relativelyPrime : relativelyPrimes) {
            long temp = ExtendedEuclideanAlgorithm.calculate(M / relativelyPrime, relativelyPrime).getS();
            while (temp < 0)
                temp += M;
            modInverse.add(temp);
        }
        for (int i = 0; i < relativelyPrimes.length; i++) {
            C = (C + (c[i] * (((M / relativelyPrimes[i]) * modInverse.get(i)) % M))) % M;
            D = (D + (d[i] * (((M / relativelyPrimes[i]) * modInverse.get(i) % M)))) % M;
        }

        return new SumMultiply(C, D);
    }

    static SumMultiply Naive(BigInteger a, BigInteger b, long[] relativelyPrimes) {
        long M = 1;
        for (Long i : relativelyPrimes) {
            M = M * i;
        }
        long c = (a.add(b)).mod(BigInteger.valueOf(M)).longValue();
        long d = (a.multiply( b).mod(BigInteger.valueOf( M))).longValue();
        return new SumMultiply(c, d);
    }

}
