import java.math.BigInteger;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println("\nWelcome to Assignment 4....\n1)Fast exponentiation\n2)Extended Euclidean Algorithm\n3)Chinese Remainder Theorem\n4)Prime number generation\n5)Quit\n");
            System.out.println("Please Enter a valid choice >> ");
            long choice;
            try {
                choice = scan.nextLong();
            } catch (InputMismatchException e) {
                scan.next();
                continue;
            }
            if (choice > 5 || choice < 1) {
                continue;
            }
            if (choice == 1) {
                long t1, t2, t3, t4;
                long s1, e1s2, e2s3, e3s4, e4s5;
                long a = -1, b = -1, m = -1;
                while (a < 0) {
                    try {
                        System.out.println("Please enter a positive base >> ");
                        a = scan.nextLong();
                    } catch (InputMismatchException e) {
                        scan.next();

                    }
                }
                while (b < 0) {
                    try {
                        System.out.println("Please enter a positive exponent >> ");
                        b = scan.nextLong();
                    } catch (InputMismatchException e) {
                        scan.next();

                    }
                }
                while (m < 0) {
                    try {
                        System.out.println("Please enter a positive mod >> ");
                        m = scan.nextLong();
                    } catch (InputMismatchException e) {
                        scan.next();

                    }
                }
                int warmUp = 12000;
                do {
                    s1 = System.nanoTime();
                    t1 = ModExponentiation.firstNaive(a, b, m);
                    e1s2 = System.nanoTime();
                    warmUp--;
                } while (warmUp > 0);
                System.out.println("Using Naive 1: (" + a + "^" + b + ")%" + m + " = " + t1 + " in time = " + (e1s2 - s1) + " ns");
                warmUp = 12000;
                do {
                    e1s2 = System.nanoTime();
                    t2 = ModExponentiation.secondNaive(a, b, m);
                    e2s3 = System.nanoTime();
                    warmUp--;
                } while (warmUp > 0);
                System.out.println("Using Naive 2: (" + a + "^" + b + ")%" + m + " = " + t2 + " in time = " + (e2s3 - e1s2) + " ns");
                warmUp = 12000;
                do {
                    e2s3 = System.nanoTime();
                    t3 = ModExponentiation.fastExpRec(a, b, m);
                    e3s4 = System.nanoTime();
                    warmUp--;
                } while (warmUp > 0);
                System.out.println("Using recursive mod exp: (" + a + "^" + b + ")%" + m + " = " + t3 + " in time = " + (e3s4 - e2s3) + " ns");
                warmUp = 12000;
                do {
                    e3s4 = System.nanoTime();
                    t4 = ModExponentiation.fastExpIter(a, b, m);
                    e4s5 = System.nanoTime();
                    warmUp--;
                } while (warmUp > 0);
                System.out.println("Using iterative mod exp: (" + a + "^" + b + ")%" + m + " = " + t4 + " in time = " + (e4s5 - e3s4) + " ns");
                continue;
            }
            if (choice == 2) {
                long a = -1, b = -1;
                while (a < 0) {
                    try {
                        System.out.println("Please enter a positive number >> ");
                        a = scan.nextLong();
                    } catch (InputMismatchException e) {
                        scan.next();

                    }
                }
                while (b < 0) {
                    try {
                        System.out.println("Please enter another positive number >> ");
                        b = scan.nextLong();
                    } catch (InputMismatchException e) {
                        scan.next();

                    }
                }
                ExtendedEuclideanAlgorithm.GCDSet result = ExtendedEuclideanAlgorithm.calculate(a, b);
                System.out.println(result.getGcd() + " = (" + result.getS() + ")" + a + " + (" + result.getT() + ")" + b);
                continue;
            }
            if (choice == 3) {
                ChineseRemainderTheorem.SumMultiply naive, crt;
                long e5s6, e6s7, e7;
                String a = "a", b = "b";
                BigInteger A = BigInteger.valueOf(1), B = BigInteger.valueOf(1);
                while (a.equals("a")) {
                    try {
                        System.out.println("Please enter a positive number >> ");
                        a = scan.next();
                        A = new BigInteger(a);
                        if (A.compareTo(BigInteger.valueOf(0)) < 0)
                            throw new RuntimeException();

                    } catch (RuntimeException s) {

                    } catch (Exception e) {
                        scan.next();

                    }
                }
                while (b.equals("b")) {
                    try {
                        System.out.println("Please enter another positive number >> ");
                        b = scan.next();
                        B = new BigInteger(b);
                        if (B.compareTo(BigInteger.valueOf(0)) < 0)
                            throw new RuntimeException();
                    } catch (RuntimeException s) {

                    } catch (Exception e) {
                        scan.next();

                    }
                }
                int n = -1;
                while (n < 0) {
                    try {
                        System.out.println("Please enter the size of relatively primes set >>");
                        n = scan.nextInt();
                    } catch (InputMismatchException e) {
                        scan.next();

                    }

                }
                System.out.println("Please enter the numbers of the relatively primes set >>");
                long[] rel = new long[n];
                while (true) {
                    for (int i = 0; i < n; i++) {
                        try {
                            rel[i] = scan.nextLong();
                        } catch (InputMismatchException e) {
                            System.out.println("please enter valid input !!! \n");
                            i--;
                        }
                    }
                    int i;
                    for (i = 0; i < n - 1; i++) {
                        if (ExtendedEuclideanAlgorithm.calculate(rel[i], rel[i + 1]).getGcd() != 1) {
                            System.out.println("Numbers entered aren't relatively prime!, please renter valid ones!\n");
                            break;
                        }
                    }
                    if (i == n - 1) {
                        break;
                    }
                }

                int warmUp = 12000;
                do {
                    e5s6 = System.nanoTime();
                    naive = ChineseRemainderTheorem.Naive(A, B, rel);
                    e6s7 = System.nanoTime();
                    warmUp--;

                }

                while (warmUp > 0);
                System.out.println("Using naive method\n" + "a+b = " + naive.getSum() + " | a*b = " + naive.getMultiply() + " in time = " + (e6s7 - e5s6) + " ns");

                warmUp = 12000;
                do {
                    e6s7 = System.nanoTime();
                    crt = ChineseRemainderTheorem.CRT(A, B, rel);
                    e7 = System.nanoTime();
                    warmUp--;

                } while (warmUp > 0);
                System.out.println("Using crt method\n" + "a+b = " + crt.getSum() + " | a*b = " + crt.getMultiply() + " in time = " + (e7 - e6s7) + " ns");

                continue;
            }
            if (choice == 4) {
                long a = -1, s, e;
                while (a < 0) {
                    try {
                        System.out.println("Please enter a positive number to generate primes up to it >> ");
                        a = scan.nextInt();
                    } catch (InputMismatchException l) {
                        scan.next();

                    }
                }
                ArrayList<Integer> primes;
                int warmUp = 12000;
                do {
                    warmUp--;
                    s = System.nanoTime();
                    primes = PrimeNumberGeneration.sieveOfEratosthenes((int) a);
                    e = System.nanoTime();
                } while (warmUp > 0);

                System.out.println("Prime Numbers in range = " + primes);
                System.out.println("generated in time = " + (e - s) + " ns");
            }
            if (choice == 5) {
                System.exit(0);
            }
        }


    }
}
