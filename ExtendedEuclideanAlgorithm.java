 class ExtendedEuclideanAlgorithm {

     static class GCDSet{
        private long gcd;
        private long s;
        private long t;

        private GCDSet(long gcd, long s, long t) {
            this.gcd = gcd;
            this.s = s;
            this.t = t;
        }

         long getGcd() {
            return gcd;
        }

         long getS() {
            return s;
        }

         long getT() {
            return t;
        }
    }

     static GCDSet  calculate(long no1,long no2){
        long r,old_r,s=0,old_s=1,t=1,old_t=0;
        r=no2;
        old_r=no1;
        while(r!=0){
            long q= old_r/r;
            long l=r,m=old_r,n=s,o=old_s,p=t,f=old_t;
            old_r=l;
            r=m-q*l;
            old_s=n;
            s=o-q*n;
            old_t=p;
            t=f-q*p;
        }
        return new GCDSet(old_r,old_s,old_t);
    }
}
