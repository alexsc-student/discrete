 class ModExponentiation {

     static long firstNaive(long a, long b, long m) {
        long toBeReturn = 1;
        for (long i = 0; i < b; i++) {
            toBeReturn *= a;
        }
        return toBeReturn % m;
    }

    static long secondNaive(long a, long b, long m) {
        long toBeReturn = 1;
        for (long i = 0; i < b; i++) {
            toBeReturn = (toBeReturn * a) % m;
        }
        return toBeReturn;
    }

    static long fastExpRec(long a, long b, long m) {
        if (b == 0) {
            return 1;
        }
        long temp = (fastExpRec(a, b>>1, m) % m);
        if ((b & 1) == 0) {
            return (temp * temp) % m;
        } else {
            return (((temp * temp) % m) * (a % m)) % m;
        }
    }

    static long fastExpIter(long a, long b, long m) {
        a %= m;
        long toBeReturn = 1;
        while (b > 0) {
            if ((b & 1) == 1) {
                toBeReturn = (toBeReturn * a) % m;
            }
            b >>= 1;
            a = (a * a) % m;
        }

        return toBeReturn;
    }


}
